package org.graffitidiary.utils;

public class FusionMessageType {

    public interface IntentRequstCode{
        int base=0;
        //地址选择页面
        int activity_select_address=base+1;
        //选择门店页面
        int activity_select_stores=base+2;
        //查看图片页面
        int activity_check_photos=base+3;
        //选择优惠券
        int activity_select_coupon=base+4;
        //刷新购物车
        int fragment_refresh_shopcart=base+5;
        //登录页面
        int activity_login=base+6;
        //设置页面
        int activity_setting=base+7;
        //选择收货地址
        int activity_select_user_address=base+8;
        //用户咨询页面
        int activity_problem_counseling=base+9;
        //报修
        int activity_order_repait=base+10;
        //评论
        int activity_order_comment=base+11;
        //详情
        int activity_order_details=base+12;
        //支付
        int activity_pay=base+13;
        //兑换中心
        int activity_exchange_center=base+14;
        //修改密码
        int activity_chuange_password=base+15;
        //积分兑换
        int activity_confirm_order_integral=base+16;
        //商品详情页
        int activity_product_detail=base+17;
        //积分商品详情页
        int activity_product_detail_points=base+18;
        //提交订单
        int activity_confirm_order=base+19;
        //积分订单详情
        int activity_integral_order_details=base+20;
        //添加或编辑收货地址
        int activity__add_edit_address=base+21;
        //注册页面
        int activity_register=base+22;
    }
}
