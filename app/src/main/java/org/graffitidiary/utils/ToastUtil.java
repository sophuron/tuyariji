package org.graffitidiary.utils;

import org.graffitidiary.GraffitiDiaryApplication;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;


/**
 * 用来全局显示Toast的类
 * @author wang
 *
 */
public class ToastUtil {

    private static android.widget.Toast mToast = null;

    /** 保证在UI线程中显示Toast */
    private static Handler mHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            if (mToast != null) {
                mToast.cancel();
            }
	            /*if (GloableParams.loginState==GloableParams.LOGINSTATE_TOKEN_INVALID) {
	            	GloableParams.loginState=GloableParams.LOGINSTATE_LOGOUT;

	            	msg.obj="用户信息已失效,请重新登录";
				}*/
            CharSequence text = (CharSequence) msg.obj;
            int duration = msg.arg2;
            mToast = Toast.makeText(GraffitiDiaryApplication.CONTEXT, text, duration);
            mToast.show();

        }
    };


    public static void showToast(String text,String keyword,int color,int duration) {
        showToast(KeywordUtil.matcherSearchTitle(color, text, keyword), duration);
    }

    public static void showShortToast(String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    public static void showShortToast(int textResId) {
        showToast(textResId, Toast.LENGTH_SHORT);
    }

    public static void showLongToast(String text) {
        showToast(text, Toast.LENGTH_LONG);
    }

    public static void showLongToast(int textResId) {
        showToast(textResId, Toast.LENGTH_LONG);
    }

    public static void showToast(int textResId, int duration) {
        showToast(GraffitiDiaryApplication.CONTEXT.getString(textResId), duration);
    }

    public static void showToast(CharSequence text, int duration) {
        mHandler.sendMessage(mHandler.obtainMessage(0, 0, duration, text));
    }
}
