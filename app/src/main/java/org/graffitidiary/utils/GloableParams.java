package org.graffitidiary.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

public class GloableParams {
    /**
     * 代理ip
     */
    public static String PROXY_IP = "http://123.206.218.172/GraffitiDiaryDemo/";
    /**
     * 代理端口
     */
    public static int PROXY_PORT = 0;

    /**
     * 屏幕宽度
     */
    public static int WINDOW_WIDTH = 0;


    public static int WINDOW_HEIGHT = 0;

    /**
     * 发送验证码等待时间
     */
    public static int  VerificationCodeMillisInFuture=60*1000;
    /**
     * 用户ID
     */
    public static int USERID=-1;

    /**
     * 用户头像url
     */
    public static String UserHeadPortraitUrl="";
    /**
     * 设备号
     */
    public static String RegistrationID="";


    /**
     * 用户定位信息
     */
//	public static AMileBeanLocation location;

    /**
     * 用户token
     */
    public static String  token="";

    /**
     * 用户Id
     */
    public static int id;

    /**用户类型*/
    public static String userType="";

    /**
     * 用户登录状态
     */

    public static final int LOGINSTATE_LOGIN=1;
    public static final int LOGINSTATE_LOGOUT=2;
    public static final int LOGINSTATE_TOKEN_INVALID=3;
    public static int loginState=0;

    /**
     * 本地文件存放位置
     */
    public static String localFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GraffitiDiary/data/";
    public static String netFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GraffitiDiary/net/";

//	/**
//	 * 用户余额
//	 */
//	public static Float MONEY = 0f;
//	/**收藏数*/
//	public static int favCount=0;
//	/**积分数*/
//	public static long pointsCount=0;
//	/**优惠券*/
//	public static long couponCount=0;
//	/**剩余消息未读数*/
//	public static long messageNum=0;
    /**
     * wifi下是否自动更新
     */
    public static boolean isWIFIUpdate=false;

    /**
     * 从哪个界面去登陆界面,好跳转回来
     */
    public static Class toLoginActivity ;

    /**
     * 要关闭的界面。
     */
    public  static Activity toCloseActivity ;

    public static Context CONTEXT;

    public static long RequestID;

    public static boolean SUBMITOK = false;
//	/**
//	 * 设为默认地址
//	 */
//	public static boolean SETDEFAULTADDRESS = false;

//	/**
//	 * 热门搜索关键词
//	 */
//	public static String[] hotKeys ;
//
//	public static boolean hasHotKey = false ; // true 为已经联网获取过关键词
//
//	public static boolean hasCategory = false ; // true 为已经联网获取过分类
//
//	public static ArrayList<Category> categoryInfos;// true 联网分类信息
//
//	public static int PRODUCTID2 = 0;

    // 额外类
    public static class Extra {
        public static final String IMAGES = "com.nostra13.example.universalimageloader.IMAGES";
        public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
    }
    //判断用户是否登录
    public static boolean isLogin() {
        return USERID>0 && !TextUtils.isEmpty(token) ;
    }
}
