package org.graffitidiary.utils;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

public class ActivityCollector {
    public static List<Activity> activities = new ArrayList<Activity>();
    private static ActivityCollector instance;

    private ActivityCollector() {
    }

    public static ActivityCollector getInstance() {
        if (null == instance) {
            instance = new ActivityCollector();
        }
        return instance;
    }

    public static void addActivity(Activity activity){
        activities.add(activity);
    }
    public static void removeActivity(Activity activity){
        activities.remove(activity);
    }
    public boolean haveActivity(Class<? extends Activity> have){
        return activities.contains(have);
    }

    public static void finishAll(){
        for (Activity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }
    /**
     * 关闭所有页面
     * @param exclude 不包括的页面
     */
    public static void finishAll(Class<? extends Activity> exclude){
        for (Activity activity : activities) {
            if (!activity.isFinishing()) {
                if (activity.getClass()!=exclude) {
                    activity.finish();
                }
            }
        }
    }
}
