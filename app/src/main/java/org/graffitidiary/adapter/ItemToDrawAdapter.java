package org.graffitidiary.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.graffitidiary.R;
import org.graffitidiary.bean.FriendList.Informationlist;
import org.graffitidiary.framework.adapter.BaseCommAdapter;
import org.graffitidiary.framework.adapter.ViewHolder;
import org.graffitidiary.utils.GloableParams;
import org.graffitidiary.utils.RelativeDateFormat;

import java.util.List;

/**
 * Created by 70339 on 2016/12/2.
 */

public class ItemToDrawAdapter extends BaseCommAdapter<Informationlist> {
    private List<Informationlist> datas;
    public ItemToDrawAdapter(List<Informationlist> datas) {
        super(datas);
        this.datas = datas;
    }

    @Override
    protected void setUI(ViewHolder holder, int position, Context context) {
        Informationlist item = getItem(position);

        LinearLayout ll_right = holder.getItemView(R.id.ll_right);
        LinearLayout ll_left = holder.getItemView(R.id.ll_left);
        TextView tv_left_name = holder.getItemView(R.id.tv_left_name);
        TextView tv_right_name = holder.getItemView(R.id.tv_right_name);
        TextView tv_date = holder.getItemView(R.id.tv_date);

        String dateTime = RelativeDateFormat.format(item.getExchangetime());

        if(position > 0){
            Informationlist item0 = getItem(position - 1);
            long sub = (item.getExchangetime().getTime() - item0.getExchangetime().getTime())/(1000*60);
            if (sub > 10) {
                tv_date.setText(dateTime);
                tv_date.setVisibility(View.VISIBLE);
            } else {
                tv_date.setVisibility(View.GONE);
            }
        }else{
            tv_date.setText(dateTime);
            tv_date.setVisibility(View.VISIBLE);
        }

        if(item.getAUid() != GloableParams.id){
            ll_right.setVisibility(View.VISIBLE);
            tv_right_name.setText(item.getGraffitiname());
            ll_left.setVisibility(View.GONE);
        }else{
            ll_left.setVisibility(View.VISIBLE);
            tv_left_name.setText(item.getGraffitiname());
            ll_right.setVisibility(View.GONE);
        }

    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_to_draw;
    }
}
