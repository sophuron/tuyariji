package org.graffitidiary.adapter;

import java.util.ArrayList;
import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.activity.FileListActivity;
import org.graffitidiary.activity.LoginActivity;
import org.graffitidiary.bean.FileInfo;
import org.graffitidiary.framework.adapter.BaseCommAdapter;
import org.graffitidiary.framework.adapter.ViewHolder;
import org.graffitidiary.utils.GloableParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.ProgressCallback;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ItemFileAdapter extends BaseCommAdapter<FileInfo>{
    private List<FileInfo> datas;
    //得到文件路径
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GraffitiDiary/data/";

    public ItemFileAdapter(List<FileInfo> datas) {
        super(datas);
        this.datas = datas;
    }

    @Override
    protected void setUI(ViewHolder holder, int position, final Context context) {
        final FileInfo item = getItem(position);
        TextView tv_file_name = holder.getItemView(R.id.tv_file_name);
        tv_file_name.setText(item.getFileNick());

        ImageView iv_file_image = holder.getItemView(R.id.iv_file_image);
        if(item.getFileState() == 0){
            iv_file_image.setImageResource(R.drawable.nosend);
        }else if(item.getFileState() == 1){
            iv_file_image.setImageResource(R.drawable.send);
        }
//		TextView tv_file_date = holder.getItemView(R.id.tv_file_date);
//		tv_file_date.setText(item.getFileDate());
//
//		TextView tv_file_size = holder.getItemView(R.id.tv_file_size);
//		tv_file_size.setText(item.getFileSize());


//		final Button btn_upload = holder.getItemView(R.id.btn_upload);
//		btn_upload.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if(null == GloableParams.token || "".equals(GloableParams.token)){
//					Intent intent = new Intent(context, LoginActivity.class);
//					context.startActivity(intent);
//				}else{
//					uploadFile(item.getFilePath(), progressBar, btn_upload);
//				}
//			}
//		});
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_file;
    }

}
