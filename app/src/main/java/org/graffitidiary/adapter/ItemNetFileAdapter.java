package org.graffitidiary.adapter;

import java.util.ArrayList;
import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.activity.FileListActivity;
import org.graffitidiary.activity.LoginActivity;
import org.graffitidiary.bean.Diarylist;
import org.graffitidiary.bean.FileInfo;
import org.graffitidiary.framework.adapter.BaseCommAdapter;
import org.graffitidiary.framework.adapter.ViewHolder;
import org.graffitidiary.utils.GloableParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.ProgressCallback;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ItemNetFileAdapter extends BaseCommAdapter<Diarylist>{
    private List<Diarylist> datas;
    //得到文件路径
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GraffitiDiary/data";

    public ItemNetFileAdapter(List<Diarylist> datas) {
        super(datas);
        this.datas = datas;
    }

    @Override
    protected void setUI(ViewHolder holder, int position, final Context context) {
        final Diarylist item = getItem(position);
        TextView tv_file_name = holder.getItemView(R.id.tv_file_name);
        tv_file_name.setText(item.getGraffitiname());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_file;
    }
}
