package org.graffitidiary.adapter;

import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.bean.Friendlist;
import org.graffitidiary.framework.adapter.BaseCommAdapter;
import org.graffitidiary.framework.adapter.ViewHolder;
import org.graffitidiary.utils.RelativeDateFormat;

import android.content.Context;
import android.widget.TextView;

public class ItemFriendsAdapter extends BaseCommAdapter<Friendlist>{
    private List<Friendlist> datas;
    public ItemFriendsAdapter(List<Friendlist> datas) {
        super(datas);
        this.datas = datas;
    }

    @Override
    protected void setUI(ViewHolder holder, int position, Context context) {
        final Friendlist item = getItem(position);

        TextView tv_friend_lastname = holder.getItemView(R.id.tv_friend_lastname);
        tv_friend_lastname.setText(item.getLastname());

        TextView tv_friend_date = holder.getItemView(R.id.tv_friend_date);
        tv_friend_date.setText(RelativeDateFormat.format(item.getRefreshtime()));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_friends;
    }

}
