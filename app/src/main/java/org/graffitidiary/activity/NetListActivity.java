package org.graffitidiary.activity;

import static com.okhttplib.annotation.CacheLevel.FIRST_LEVEL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.adapter.ItemFileAdapter;
import org.graffitidiary.adapter.ItemNetFileAdapter;
import org.graffitidiary.bean.Diary;
import org.graffitidiary.bean.Diarylist;
import org.graffitidiary.framework.ui.ActionBarActivity;
import org.graffitidiary.utils.GloableParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.CallbackOk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class NetListActivity extends ActionBarActivity{
    private ListView lv_files;
    ItemNetFileAdapter itemNetFileAdapter;
    String path;//得到文件路径
    List<Diarylist> list2 = new ArrayList<Diarylist>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBar();
        setBarTitle("网络涂鸦");
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_net_file;
    }

    @Override
    protected void initViews() {
        lv_files = (ListView) findViewById(R.id.lv_net_files);
        doHttpAsync();

        lv_files.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String filePath = GloableParams.PROXY_IP + list2.get(position).getUploadedfile();
                Intent intent = new Intent(NetListActivity.this, AutoDrawActivity.class);
                intent.putExtra("Name", list2.get(position).getGraffitiname());
                intent.putExtra("fileName", "bsnd");
                intent.putExtra("filePath", filePath);
                startActivity(intent);
            }
        });
    }

    /**
     * 异步请求：回调方法可以直接操作UI
     */
    private void doHttpAsync() {
        OkHttpUtil okHttpUtil = OkHttpUtil.Builder()
                .setCacheLevel(FIRST_LEVEL)
                .setConnectTimeout(25).build(this);
        okHttpUtil.doGetAsync(
                HttpInfo.Builder().setUrl(GloableParams.PROXY_IP + "FindAllDiary.php").build(),
                new CallbackOk() {
                    @Override
                    public void onResponse(HttpInfo info) throws IOException {
                        if (info.isSuccessful()) {//,false
                            Diary diary = JSON.parseObject(info.getRetDetail().replace(",false", ""), Diary.class);
                            showDiaryList(diary.getDiarylist());
                        }
                    }
                });
    }


    private void showDiaryList(List<Diarylist> list1) {
        list2.addAll(list1);
        itemNetFileAdapter = new ItemNetFileAdapter(list2);
        lv_files.setAdapter(itemNetFileAdapter);
    }

}
