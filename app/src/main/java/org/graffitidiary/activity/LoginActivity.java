package org.graffitidiary.activity;

import static com.okhttplib.annotation.CacheLevel.FIRST_LEVEL;

import java.io.IOException;
import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.bean.Userinfo;
import org.graffitidiary.framework.ui.ActionBarActivity;


import org.graffitidiary.manager.Dbs;
import org.graffitidiary.utils.GloableParams;
import org.graffitidiary.utils.ToastUtil;
import org.graffitidiary.view.CustomEditTextView;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.DbUtils.DaoConfig;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.CallbackOk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends ActionBarActivity{
    private Button btn_login;
    private TextView tv_regist;
    private TextView tv_qqlogin;

    private CustomEditTextView c_username;
    private CustomEditTextView c_password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBar();
        setBarTitle("登录");
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        tv_regist = (TextView) findViewById(R.id.tv_regist);
        tv_regist.setOnClickListener(this);
        tv_qqlogin = (TextView) findViewById(R.id.tv_qqlogin);
        tv_qqlogin.setOnClickListener(this);

        c_username = (CustomEditTextView) findViewById(R.id.cetv_username_login);
        c_username.setLeftImagePressAndNomal(R.drawable.icon_username_press, R.drawable.icon_username_normal);
        c_username.setEditHideText("请输入手机号");
        c_password = (CustomEditTextView) findViewById(R.id.cetv_password_login);
        c_password.setLeftImagePressAndNomal(R.drawable.icon_password_press, R.drawable.icon_password_normal);
        c_password.setEditHideText("请输入密码");
        c_password.hideBottomBar();
        c_password.setEditHideText();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_login:
                String username = c_username.getEditText();
                String password = c_password.getEditText();

                if (username.equals("") || password.equals("")) {
                    ToastUtil.showShortToast("用户名或密码不能为空");
                    return;
                }
                if (password.length()<6 || password.length() > 15) {
                    ToastUtil.showShortToast("密码长度为6位~15位");
                    return;
                }
                showProgressDialog();
                Login(username, password);
                break;
            case R.id.tv_regist:
                startIntent(RegisterActivity.class);
                break;
            case R.id.tv_qqlogin:
                ToastUtil.showLongToast(R.string.no_qqlogin);
                break;
        }

    }

    private void Login(String username, String password) {

        OkHttpUtil okHttpUtil = OkHttpUtil.Builder()
                .setCacheLevel(FIRST_LEVEL)
                .setConnectTimeout(25).build(this);
        okHttpUtil.doPostAsync(
                HttpInfo.Builder().setUrl(GloableParams.PROXY_IP + "login.php")
                        .addParam("username", username)
                        .addParam("password", password)
                        .build(),
                new CallbackOk() {
                    @Override
                    public void onResponse(HttpInfo info) throws IOException {
                        closeProgressDialog();
                        if (info.isSuccessful()) {
                            String result = info.getRetDetail();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                if("200".equals(jsonObject.getString("code"))){
                                    String user = jsonObject.getString("userinfo");
                                    Userinfo userinfo = JSON.parseObject(user, Userinfo.class);
                                    insertUserInfo(userinfo);
                                    finish();
                                }else{
                                    String message = jsonObject.getString("message");
                                    ToastUtil.showShortToast(message);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            ToastUtil.showShortToast(info.getRetDetail());
                        }
                    }

                });
    }

    private void insertUserInfo(Userinfo user) {
        user.setId(1);
        Dbs.getDb(this);
        try {
            Dbs.getDb().saveOrUpdate(user);
            Dbs.close();
        } catch (DbException e) {
            e.printStackTrace();
        }

        GloableParams.token = user.getToken();
    }

//http://123.206.218.172/GraffitiDiaryDemo/FindAllDiary.php
}
