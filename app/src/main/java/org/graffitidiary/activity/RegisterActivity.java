package org.graffitidiary.activity;

import static com.okhttplib.annotation.CacheLevel.FIRST_LEVEL;

import java.io.IOException;

import org.graffitidiary.R;
import org.graffitidiary.bean.Userinfo;
import org.graffitidiary.framework.ui.ActionBarActivity;
import org.graffitidiary.utils.GloableParams;
import org.graffitidiary.utils.ToastUtil;
import org.graffitidiary.view.CustomEditTextView;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.CallbackOk;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends ActionBarActivity{
    private CustomEditTextView r_username;
    private CustomEditTextView r_password;

    private Button btn_register;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBar();
        setBarTitle("注册");
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_register;

    }

    @Override
    protected void initViews() {
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);

        r_username = (CustomEditTextView) findViewById(R.id.cetv_username_regist);
        r_username.setLeftImagePressAndNomal(R.drawable.icon_username_press, R.drawable.icon_username_normal);
        r_username.setEditHideText("请输入手机号");
        r_password = (CustomEditTextView) findViewById(R.id.cetv_password_regist);
        r_password.setLeftImagePressAndNomal(R.drawable.icon_password_press, R.drawable.icon_password_normal);
        r_password.setEditHideText("请输入密码");
        r_password.hideBottomBar();
        r_password.setEditHideText();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_register:
                String username = r_username.getEditText();
                String password = r_password.getEditText();
                if (username.equals("") || password.equals("")) {
                    ToastUtil.showShortToast("用户名或密码不能为空");
                    return;
                }
                if (password.length()<6 || password.length() > 15) {
                    ToastUtil.showShortToast("密码长度为6位~15位");
                    return;
                }
                showProgressDialog();
                register(username, password);
                break;
        }
    }

    private void register(String username, String password) {
        OkHttpUtil okHttpUtil = OkHttpUtil.Builder()
                .setCacheLevel(FIRST_LEVEL)
                .setConnectTimeout(25).build(this);
        okHttpUtil.doPostAsync(
                HttpInfo.Builder().setUrl(GloableParams.PROXY_IP + "regist.php")
                        .addParam("username", username)
                        .addParam("password", password)
                        .build(),
                new CallbackOk() {
                    @Override
                    public void onResponse(HttpInfo info) throws IOException {
                        closeProgressDialog();
                        if (info.isSuccessful()) {
                            String result = info.getRetDetail();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                if("200".equals(jsonObject.getString("code"))){
                                    ToastUtil.showShortToast("注册成功");
                                    finish();
                                }else{
                                    String message = jsonObject.getString("message");
                                    ToastUtil.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            ToastUtil.showShortToast(info.getRetDetail());
                        }
                    }

                });
    }

}
