package org.graffitidiary.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.CallbackOk;

import org.graffitidiary.R;
import org.graffitidiary.framework.ui.ActionBarActivity;
import org.graffitidiary.utils.GloableParams;
import org.graffitidiary.utils.ToastUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.okhttplib.annotation.CacheLevel.FIRST_LEVEL;

/**
 * Created by 70339 on 2016/12/6.
 */

public class FeedbackActivity extends ActionBarActivity {

    EditText et_feedback;
    Button btn_send;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBar();
        setBarTitle("反馈");
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void initViews() {
        et_feedback = (EditText) findViewById(R.id.et_feedback);
        btn_send = (Button) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_send:
                String feedback = et_feedback.getText().toString().trim();
                if(!TextUtils.isEmpty(feedback)){

                    showProgressDialog();
                    send(feedback);
                }else{
                    ToastUtil.showLongToast("反馈不能为空");
                }
                break;
        }
    }

    private void send(String feedback) {
        OkHttpUtil okHttpUtil = OkHttpUtil.Builder()
                .setCacheLevel(FIRST_LEVEL)
                .setConnectTimeout(25).build(this);
        okHttpUtil.doPostAsync(
                HttpInfo.Builder().setUrl(GloableParams.PROXY_IP + "AddFeedBack.php")
                        .addParam("token", GloableParams.token)
                        .addParam("content", feedback)
                        .build(),
                new CallbackOk() {
                    @Override
                    public void onResponse(HttpInfo info) throws IOException {
                        closeProgressDialog();
                        if (info.isSuccessful()) {
                            finish();
                        } else {
                            ToastUtil.showShortToast(info.getRetDetail());
                        }
                    }



                });
    }
}
