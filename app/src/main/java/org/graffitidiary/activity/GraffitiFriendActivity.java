package org.graffitidiary.activity;

import static com.okhttplib.annotation.CacheLevel.FIRST_LEVEL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.graffitidiary.R;
import org.graffitidiary.adapter.ItemFriendsAdapter;
import org.graffitidiary.bean.Friendlist;
import org.graffitidiary.bean.Friendlists;
import org.graffitidiary.bean.Userinfo;
import org.graffitidiary.framework.ui.ActionBarActivity;
import org.graffitidiary.utils.GloableParams;
import org.graffitidiary.utils.ToastUtil;
import org.graffitidiary.view.iosdialog.ActionSheetDialog;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.okhttplib.HttpInfo;
import com.okhttplib.OkHttpUtil;
import com.okhttplib.callback.CallbackOk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GraffitiFriendActivity extends ActionBarActivity {
    private ListView listview;
    LinearLayout noTuYaShow;

    List<Friendlist> list = new ArrayList<Friendlist>();//存放全部数据
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBar();
        setBarTitle("涂友列表");
        showBarRightImgBtn();
        setBarRightBackgroundResource(R.drawable.find);
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_graffiti_friend;
    }

    @Override
    protected void initViews() {
        listview = (ListView) findViewById(R.id.lv_files);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GloableParams.id = list.get(i).getA_uid();
                Intent intent = new Intent(GraffitiFriendActivity.this, ToDrawActivity.class);
                intent.putExtra("s_uid", list.get(i).getS_uid() + "");
                startActivity(intent);
            }
        });

        noTuYaShow = (LinearLayout) findViewById(R.id.ll_no_file_tip);
        showProgressDialog();
        getTuYaFriend();
    }

    private void getTuYaFriend() {
        OkHttpUtil okHttpUtil = OkHttpUtil.Builder()
                .setCacheLevel(FIRST_LEVEL)
                .setConnectTimeout(25).build(this);
        okHttpUtil.doPostAsync(
                HttpInfo.Builder().setUrl(GloableParams.PROXY_IP + "GetFriendList.php")
                        .addParam("token", GloableParams.token)
                        .build(),
                new CallbackOk() {
                    @Override
                    public void onResponse(HttpInfo info) throws IOException {
                        closeProgressDialog();
                        if (info.isSuccessful()) {
                            Friendlists friendlists = JSON.parseObject(info.getRetDetail(), Friendlists.class);
                            if("200".equals(friendlists.getCode())){
                                list.addAll(friendlists.getFriendlist());
                                display(list);
                            } else if ("202".equals(friendlists.getCode())) {
                                ToastUtil.showLongToast("登陆失效请重新登陆");
                                startIntent(LoginActivity.class);
                                finish();
                            } else{
                                noTuYaShow.setVisibility(View.VISIBLE);
                            }
                        } else {
                            ToastUtil.showShortToast(info.getRetDetail());
                        }
                    }



                });
    }

    boolean isAdapter = true;
    ItemFriendsAdapter friendsAdapter;
    private void display(List<Friendlist> list) {

        if(isAdapter){
            friendsAdapter = new ItemFriendsAdapter(list);
            listview.setAdapter(friendsAdapter);
            listview.setVisibility(View.VISIBLE);
            isAdapter = false;
        }else{
            friendsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.bar_btn_right:
                showMore();
                break;
        }
    }

    private void showMore() {
        new ActionSheetDialog(GraffitiFriendActivity.this)
                .builder()
                .setTitle("更多")
                .addSheetItem("反馈", ActionSheetDialog.SheetItemColor.Orange,
                        new ActionSheetDialog.OnSheetItemClickListener() {
                            @Override
                            public void onClick(int which) {
                                startIntent(FeedbackActivity.class);
                            }
                        }).show();
    }
}
