package org.graffitidiary.view.refresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by kuma on 2016/12/5.
 */

public class RefreshListView extends ListView {
    private View footrView;

    public RefreshListView(Context context) {
        super(context);
    }

    public RefreshListView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        footrView = new View(getContext());
        addFooterView(footrView);
        super.setAdapter(adapter);
        removeFooterView(footrView);
    }

}

