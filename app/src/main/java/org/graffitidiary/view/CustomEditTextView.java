package org.graffitidiary.view;


import org.graffitidiary.R;

import android.content.Context;
import android.view.View.OnClickListener;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnFocusChangeListener;

public class CustomEditTextView extends LinearLayout implements OnFocusChangeListener, OnClickListener{
    Context context;
    ImageView imageViewLeft;
    EditText editText;
    ImageButton imageViewRight;
    TextView tv_bottom;

    public CustomEditTextView(Context context) {
        super(context);
    }

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        control();
    }

    private void control()
    {
        initialise(); // 实例化内部view
        setViewListener();
    }

    private void initialise() {
        LayoutInflater.from(context).inflate(R.layout.ll_custom_edittext, this);
    }

    private void setViewListener() {
        imageViewLeft = (ImageView) findViewById(R.id.iv_left);
        editText = (EditText) findViewById(R.id.et_center);
        imageViewRight = (ImageButton) findViewById(R.id.iv_right);
        imageViewRight.setOnClickListener(this);
        tv_bottom = (TextView) findViewById(R.id.tv_bottom);

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0){
                    imageViewLeft.setImageResource(pressImage);
                    imageViewRight.setVisibility(View.VISIBLE);
                }else{
                    imageViewLeft.setImageResource(nomalImage);
                    imageViewRight.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_center:
                if(hasFocus){
                    imageViewLeft.setImageResource(pressImage);
                }else{
                    imageViewLeft.setImageResource(nomalImage);
                }
                break;

            default:
                break;
        }
    }

    public String getEditText() {
        return editText.getText().toString().trim();
    }

    int pressImage;
    int nomalImage;
    public void setLeftImagePressAndNomal(int press, int nomal) {
        this.pressImage = press;
        this.nomalImage = nomal;
        imageViewLeft.setImageResource(nomal);
    }

    /**
     * 隐藏底部条
     */
    public void hideBottomBar() {
        tv_bottom.setVisibility(View.GONE);
    }

    public void setEditHideText(String hint) {
        editText.setHint(hint);
    }

    public void setEditHideText() {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_right:
                editText.setText("");
                break;
        }
    }

}
