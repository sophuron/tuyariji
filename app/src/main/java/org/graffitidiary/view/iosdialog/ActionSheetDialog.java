package org.graffitidiary.view.iosdialog;

import java.util.ArrayList;
import java.util.List;

import org.graffitidiary.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

public class ActionSheetDialog {
    private Context context;
    private Dialog dialog;
    private TextView txt_title;
    private TextView txt_cancel;
    private LinearLayout lLayout_content;
    private ScrollView sLayout_content;
    private boolean showTitle = false;
    private List<SheetItem> sheetItemList;
    private Display display;
    private ListView mListView;
    private int itemGravity=Gravity.CENTER;
    private int itemIndex=-1;

    public ActionSheetDialog(Context context){
        this.context = context;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    public ActionSheetDialog builder(){
        //获取Dialog布局
        View view = LayoutInflater.from(context).inflate(R.layout.view_actionsheet, null);
        // 设置Dialog最小宽度为屏幕宽度
        view.setMinimumWidth(display.getWidth());
        //获取自定义Dialog布局中的控件
        sLayout_content = (ScrollView) view.findViewById(R.id.sLayout_content);
        lLayout_content = (LinearLayout) view
                .findViewById(R.id.lLayout_content);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_cancel = (TextView) view.findViewById(R.id.txt_cancel);
        mListView=(ListView) view.findViewById(R.id.view_actionsheet_listview);
        txt_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // 定义Dialog布局和参数
        dialog = new Dialog(context, R.style.ActionSheetDialogStyle);
        dialog.setContentView(view);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.x = 0;
        lp.y = 0;
        dialogWindow.setAttributes(lp);
        return this;
    }

    public ActionSheetDialog setTitle(String title){
        showTitle = true;
        txt_title.setVisibility(View.VISIBLE);
        txt_title.setText(title);
        return this;
    }

    public ActionSheetDialog setCancelable(boolean cancel){
        dialog.setCancelable(cancel);
        return this;
    }

    public ActionSheetDialog setCanceledOnTouchOutside(boolean cancel){
        dialog.setCanceledOnTouchOutside(cancel);
        return this;
    }

    public ActionSheetDialog setSelectItem(int index){
        itemIndex = index;
        return this;
    }

    public ActionSheetDialog setItemGravity(int gravity){
        itemGravity=gravity;
        return this;
    }

    public ActionSheetDialog addSheetItem(int iconResId,String strItem, SheetItemColor color,OnSheetItemClickListener listener) {
        if (sheetItemList == null) {
            sheetItemList = new ArrayList<SheetItem>();
        }
        sheetItemList.add(new SheetItem(iconResId!=0?context.getResources().getDrawable(iconResId):null,strItem, color, listener));
        return this;
    }

    public ActionSheetDialog addSheetItem(String strItem, SheetItemColor color,OnSheetItemClickListener listener) {
        if (sheetItemList == null) {
            sheetItemList = new ArrayList<SheetItem>();
        }
        sheetItemList.add(new SheetItem(null,strItem, color, listener));
        return this;
    }

    public void show() {
        setSheetItems();
        dialog.show();
    }
    /** 设置条目布局 */
    private void setSheetItems() {
        if(sheetItemList == null || sheetItemList.size() <= 0){
            return;
        }
        int size = sheetItemList.size();

        // TODO 高度控制，非最佳解决办法
        // 添加条目过多的时候控制高度
        if(size >= 7){
            LinearLayout.LayoutParams params = (LayoutParams) mListView.getLayoutParams();
            params.height = display.getHeight() / 2;
            mListView.setLayoutParams(params);
        }

        IOSDialogItemAdapter dialogItemAdapter = new IOSDialogItemAdapter(sheetItemList, context);
        dialogItemAdapter.setSelectItem(itemIndex);
        mListView.setAdapter(dialogItemAdapter);
    }

    public class SheetItem{
        String name;
        OnSheetItemClickListener itemClickListener;
        SheetItemColor color;
        Drawable icon;
        public SheetItem(Drawable icon,String name, SheetItemColor color,
                         OnSheetItemClickListener itemClickListener){
            this.icon=icon;
            this.name = name;
            this.color = color;
            this.itemClickListener = itemClickListener;
        }
    }

    public interface OnSheetItemClickListener{
        void onClick(int which);
    }

    public enum SheetItemColor {
        Orange("#f77600"), Red("#FD4A2E");
        //int gravity;
        private String name;

        private Drawable icon;

        public Drawable getIcon() {
            return icon;
        }

        public void setIcon(Drawable icon) {
            this.icon = icon;
        }

        private SheetItemColor(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    class IOSDialogItemAdapter extends BaseAdapter{
        private List<SheetItem> mListModels;
        private Context mContext;
        private LayoutInflater mInflater=null;

        public IOSDialogItemAdapter(List<SheetItem> mListModels,
                                    Context mContext) {
            super();
            this.mListModels = mListModels;
            this.mContext = mContext;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mListModels.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return mListModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView==null) {
                convertView = mInflater.inflate(R.layout.item_actionsheetdialog, null);
                holder = new ViewHolder();
                holder.mLayout=(LinearLayout) convertView.findViewById(R.id.item_actionsheetdialog_layout);
                holder.mIvIcon=(ImageView) convertView.findViewById(R.id.item_actionsheetdialog_imageview_icon);
                holder.mTvTitle=(TextView) convertView.findViewById(R.id.item_actionsheetdialog_textview_title);
                holder.mIvTick=(ImageView) convertView.findViewById(R.id.item_actionsheetdialog_imageview_tick);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (itemGravity!=Gravity.CENTER) {
                holder.mLayout.setGravity(itemGravity);
            }
            if (mListModels.get(position).icon!=null) {
                holder.mIvIcon.setImageDrawable(mListModels.get(position).icon);
                holder.mIvIcon.setVisibility(View.VISIBLE);
            }
            holder.mTvTitle.setText(mListModels.get(position).name);
            if (position == selectItem) {
                holder.mTvTitle.setTextColor(mContext.getResources().getColor(R.color.second_color));
                holder.mIvTick.setVisibility(View.VISIBLE);
            }
            else {
                holder.mTvTitle.setTextColor(mContext.getResources().getColor(R.color.black80));
                holder.mIvTick.setVisibility(View.GONE);
            }
            // 点击事件
            holder.mLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListModels.get(position).itemClickListener.onClick(position);
                    dialog.dismiss();
                }
            });

            return convertView;
        }
        public  void setSelectItem(int selectItem) {
            this.selectItem = selectItem;
        }
        private int  selectItem=-1;
        private class ViewHolder{
            LinearLayout mLayout;
            TextView mTvTitle;
            ImageView mIvIcon;
            ImageView mIvTick;
        }
    }
}