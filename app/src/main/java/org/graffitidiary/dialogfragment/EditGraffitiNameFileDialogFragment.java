package org.graffitidiary.dialogfragment;

import org.graffitidiary.R;
import org.graffitidiary.activity.AutoDrawActivity;
import org.graffitidiary.activity.MainActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;


public class EditGraffitiNameFileDialogFragment extends DialogFragment implements View.OnClickListener{
    private View view;
    Context context;//运行上下文
    private LayoutInflater mInflater;//视图容器

    private EditText editText;
    private Button btn_cancel, btn_confirm;

    public EditGraffitiNameFileDialogFragment(Context context){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);//获取那个activity
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);//无标题栏
        view = inflater.inflate(R.layout.dialog_edit, container,false);
        init();
        display();
        return view;
    }

    private void init() {
        editText = (EditText) view.findViewById(R.id.et_setHint);
        btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);
        btn_confirm = (Button) view.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);
    }

    private void display() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_confirm:
                String name = editText.getText().toString().trim();
                if(!TextUtils.isEmpty(name)){
                    ((MainActivity)context).setGraffitiName(name);
                    dismiss();
                }else{
                    Date currentTime = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                    String dateString = formatter.format(currentTime);
                    ((MainActivity)context).setGraffitiName(dateString);
                }
                break;
        }
    }
}
