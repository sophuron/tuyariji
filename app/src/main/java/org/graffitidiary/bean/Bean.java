package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.List;

public class Bean implements Serializable{
	private Brush Brush;

	public Brush getBrush() {
		return Brush;
	}

	public void setBrush(Brush brush) {
		Brush = brush;
	}
	
}
