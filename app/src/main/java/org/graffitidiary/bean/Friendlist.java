package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;

public class Friendlist implements Serializable{
	@Id(column="key")
    private int id;
    private int f_id;
    private int s_uid;
    private int a_uid;
    private Date refreshtime;
    private String lastname;
	public int getF_id() {
		return f_id;
	}
	public void setF_id(int f_id) {
		this.f_id = f_id;
	}
	public int getS_uid() {
		return s_uid;
	}
	public void setS_uid(int s_uid) {
		this.s_uid = s_uid;
	}
	public int getA_uid() {
		return a_uid;
	}
	public void setA_uid(int a_uid) {
		this.a_uid = a_uid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getRefreshtime() {
		return refreshtime;
	}
	public void setRefreshtime(Date refreshtime) {
		this.refreshtime = refreshtime;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
    
    
}