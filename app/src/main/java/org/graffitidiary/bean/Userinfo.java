package org.graffitidiary.bean;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;

public class Userinfo implements Serializable{
	@NoAutoIncrement
    private int id;
    private String token;
    private String nickname;
    private String face;
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setToken(String token) {
         this.token = token;
     }
     public String getToken() {
         return token;
     }

    public void setNickname(String nickname) {
         this.nickname = nickname;
     }
     public String getNickname() {
         return nickname;
     }

    public void setFace(String face) {
         this.face = face;
     }
     public String getFace() {
         return face;
     }

}
