package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.List;

public class Brush implements Serializable{
	private float StrokeWidth;
	private int Color;
	private List<PathXY> PathXY;
	
	public float getStrokeWidth() {
		return StrokeWidth;
	}
	public void setStrokeWidth(float strokeWidth) {
		this.StrokeWidth = strokeWidth;
	}
	public int getColor() {
		return Color;
	}
	public void setColor(int color) {
		this.Color = color;
	}
	public List<PathXY> getPathXY() {
		return PathXY;
	}
	public void setPathXY(List<PathXY> pathXY) {
		this.PathXY = pathXY;
	}
}
