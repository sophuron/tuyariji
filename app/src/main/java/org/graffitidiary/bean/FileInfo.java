package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;

public class FileInfo implements Serializable{
    @Id(column="key")
    private int id;
    private String fileNick;//文件昵称
    private String fileName;//文件名
    private String fileSize;//文件大小
    private Date fileDate;//文件日期
    private String filePath;//文件路径
    private int fileState;//文件状态0表示未上传，1表示已上传，2表示网络获取的文件

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFileNick() {
        return fileNick;
    }
    public void setFileNick(String fileNick) {
        this.fileNick = fileNick;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileSize() {
        return fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
    public Date getFileDate() {
        return fileDate;
    }
    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public int getFileState() {
        return fileState;
    }
    public void setFileState(int fileState) {
        this.fileState = fileState;
    }

}
