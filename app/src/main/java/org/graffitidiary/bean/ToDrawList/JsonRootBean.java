package org.graffitidiary.bean.ToDrawList;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * Created by 70339 on 2016/12/2.
 */

public class JsonRootBean {

    private int code;
    private String message;
    @JSONField(name="informationList")
    private List<Informationlist> informationlist;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setInformationlist(List<Informationlist> informationlist) {
        this.informationlist = informationlist;
    }
    public List<Informationlist> getInformationlist() {
        return informationlist;
    }

}