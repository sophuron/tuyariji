package org.graffitidiary.bean.ToDrawList;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by 70339 on 2016/12/2.
 */

public class Informationlist {

    @JSONField(name="e_id")
    private String eId;
    @JSONField(name="s_uid")
    private String sUid;
    @JSONField(name="a_uid")
    private String aUid;
    private String exchangetime;
    private String diarypath;
    private String graffitiname;
    public void setEId(String eId) {
        this.eId = eId;
    }
    public String getEId() {
        return eId;
    }

    public void setSUid(String sUid) {
        this.sUid = sUid;
    }
    public String getSUid() {
        return sUid;
    }

    public void setAUid(String aUid) {
        this.aUid = aUid;
    }
    public String getAUid() {
        return aUid;
    }

    public void setExchangetime(String exchangetime) {
        this.exchangetime = exchangetime;
    }
    public String getExchangetime() {
        return exchangetime;
    }

    public void setDiarypath(String diarypath) {
        this.diarypath = diarypath;
    }
    public String getDiarypath() {
        return diarypath;
    }

    public void setGraffitiname(String graffitiname) {
        this.graffitiname = graffitiname;
    }
    public String getGraffitiname() {
        return graffitiname;
    }

}