package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.List;

public class Diary extends JsonRootBean implements Serializable{
	private List<Diarylist> diarylist;

	public List<Diarylist> getDiarylist() {
		return diarylist;
	}

	public void setDiarylist(List<Diarylist> diarylist) {
		this.diarylist = diarylist;
	}
	
}
