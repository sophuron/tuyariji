package org.graffitidiary.bean;

import java.io.Serializable;
import java.util.List;

import com.lidroid.xutils.db.annotation.Id;

public class Friendlists extends JsonRootBean implements Serializable{
	
	private List<Friendlist> friendlist;

	public List<Friendlist> getFriendlist() {
		return friendlist;
	}

	public void setFriendlist(List<Friendlist> friendlist) {
		this.friendlist = friendlist;
	}
	
}
