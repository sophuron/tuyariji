package org.graffitidiary.bean.FriendList;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 70339 on 2016/11/30.
 */

public class Informationlist implements Serializable {

    @JSONField(name="e_id")
    private int eId;
    @JSONField(name="s_uid")
    private int sUid;
    @JSONField(name="a_uid")
    private int aUid;
    private Date exchangetime;
    private String diarypath;
    private String graffitiname;
    public void setEId(int eId) {
        this.eId = eId;
    }
    public int getEId() {
        return eId;
    }

    public void setSUid(int sUid) {
        this.sUid = sUid;
    }
    public int getSUid() {
        return sUid;
    }

    public void setAUid(int aUid) {
        this.aUid = aUid;
    }
    public int getAUid() {
        return aUid;
    }

    public void setExchangetime(Date exchangetime) {
        this.exchangetime = exchangetime;
    }
    public Date getExchangetime() {
        return exchangetime;
    }

    public void setDiarypath(String diarypath) {
        this.diarypath = diarypath;
    }
    public String getDiarypath() {
        return diarypath;
    }

    public void setGraffitiname(String graffitiname) {
        this.graffitiname = graffitiname;
    }

    public String getGraffitiname() {

        return graffitiname;
    }
}