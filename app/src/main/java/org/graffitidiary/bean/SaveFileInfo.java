package org.graffitidiary.bean;

public class SaveFileInfo {
    //文件保存目录
    private String saveFileDir;
    //文件保存名称
    private String saveFileName;

    String saveFileNameWithExtension;//保存文件名称：包含扩展名
    String saveFileNameCopy;//保存文件备用名称：用于文件名称冲突
    String saveFileNameEncrypt;//保存文件名称（加密后）

    public String getSaveFileDir() {
        return saveFileDir;
    }
    public void setSaveFileDir(String saveFileDir) {
        this.saveFileDir = saveFileDir;
    }
    public String getSaveFileName() {
        return saveFileName;
    }
    public void setSaveFileName(String saveFileName) {
        this.saveFileName = saveFileName;
    }
    public String getSaveFileNameWithExtension() {
        return saveFileNameWithExtension;
    }
    public void setSaveFileNameWithExtension(String saveFileNameWithExtension) {
        this.saveFileNameWithExtension = saveFileNameWithExtension;
    }
    public String getSaveFileNameCopy() {
        return saveFileNameCopy;
    }
    public void setSaveFileNameCopy(String saveFileNameCopy) {
        this.saveFileNameCopy = saveFileNameCopy;
    }
    public String getSaveFileNameEncrypt() {
        return saveFileNameEncrypt;
    }
    public void setSaveFileNameEncrypt(String saveFileNameEncrypt) {
        this.saveFileNameEncrypt = saveFileNameEncrypt;
    }

}
