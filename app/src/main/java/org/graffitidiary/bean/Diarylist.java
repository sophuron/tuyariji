package org.graffitidiary.bean;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.lidroid.xutils.db.annotation.Id;

public class Diarylist implements Serializable{
	@Id(column="key")
    private int id;
	@JSONField(name="d_id")
    private String dId;
	@JSONField(name="u_id")
    private String uId;
    private String city;
    private String uploadedfile;
    private String graffitiname;
    private String uploaddate;
    private String modificationtime;
    private String display;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getdId() {
		return dId;
	}
	public void setdId(String dId) {
		this.dId = dId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getUploadedfile() {
		return uploadedfile;
	}
	public void setUploadedfile(String uploadedfile) {
		this.uploadedfile = uploadedfile;
	}
	public String getGraffitiname() {
		return graffitiname;
	}
	public void setGraffitiname(String graffitiname) {
		this.graffitiname = graffitiname;
	}
	public String getUploaddate() {
		return uploaddate;
	}
	public void setUploaddate(String uploaddate) {
		this.uploaddate = uploaddate;
	}
	public String getModificationtime() {
		return modificationtime;
	}
	public void setModificationtime(String modificationtime) {
		this.modificationtime = modificationtime;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	    
    
}
