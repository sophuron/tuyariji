package org.graffitidiary.bean;

import java.io.Serializable;

public class PathXY implements Serializable{
    private float X;//屏幕x坐标
    private float Y;//屏幕y坐标
    public float getX() {
        return X;
    }
    public void setX(float x) {
        this.X = x;
    }
    public float getY() {
        return Y;
    }
    public void setY(float y) {
        this.Y = y;
    }

}
