package org.graffitidiary.manager;

import org.graffitidiary.GraffitiDiaryApplication;

import android.content.Context;

import com.lidroid.xutils.DbUtils;

/**
 * 为了更好的兼容6.0版本请使用XutilsDbs
 * @author Administrator
 *
 */
public class Dbs {
    public static DbUtils db = null;
    public static DbUtils getDb(Context context){
        if (db==null) {
            db=DbUtils.create(context, "GraffitiDiary.db");
        }
        db.configAllowTransaction(true);
        return db;
    }

    public static DbUtils getDb(){
        if (db==null) {
            db=DbUtils.create(GraffitiDiaryApplication.CONTEXT, "GraffitiDiary.db");
        }
        db.configAllowTransaction(true);
        return db;
    }

    public static void close(){
        if(db!=null){
            db.close();
            db=null;
        }
    }

}