package org.graffitidiary.framework.ui;

import org.graffitidiary.GraffitiDiaryApplication;
import org.graffitidiary.R;
import org.graffitidiary.activity.LoginActivity;
import org.graffitidiary.utils.ActivityCollector;
import org.graffitidiary.utils.FusionMessageType.IntentRequstCode;
import org.graffitidiary.view.iosdialog.IOSAlertDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public abstract class BaseActivity extends FragmentActivity{
    private static final String SHAREPRE_NAME = GraffitiDiaryApplication.CONTEXT.getString(R.string.app_name_e);
    private static final String TAG = "BaseActivity";

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        Log.i(TAG, "==========================BaseActivity onCreate==============================");
        super.onCreate(savedInstanceState);
        GraffitiDiaryApplication.getInstance().addActivity(this);
        ActivityCollector.addActivity(this);
        Log.i(TAG, "==========================Activity is Add==============================");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void showToast(CharSequence message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    protected void showToast(int msgResId){
        Toast.makeText(this, msgResId, Toast.LENGTH_SHORT).show();
    }

//    protected void startIntent(Class<?> cls,String... url){
//		Intent	intent=new Intent(this, cls);
//		intent.putExtra("url", url);
//		startActivity(intent);
//	}
//
//    protected void startIntentForResult(Class<?> cls,int requstCode){
//    	startActivityForResult (new Intent(this, cls), requstCode);
//    }
//
//    protected void startIntentForResult(Class<?> cls,int requestCode ,String locktype,int locktypeid){
//		Intent	intent=new Intent(this, cls);
//		intent.putExtra("locktype", locktype);
//		intent.putExtra("locktypeid", locktypeid);
//		startActivityForResult(intent, requestCode);
//	}

    /**
     *
     * @param cls
     * 需要跳转的Activity
     */
    protected void startIntent(Class<?> cls){
        Intent	intent=new Intent(this, cls);
        startActivity(intent);
    }

    protected String getSharePreferenceString(String key) {
        SharedPreferences settings = this.getSharedPreferences(SHAREPRE_NAME, 0);
        String value  = settings.getString(key, "");
        return value;
    }

    protected void putSharePreference(String key, String value) {
        SharedPreferences settings = this.getSharedPreferences(SHAREPRE_NAME, 0);
        SharedPreferences.Editor localEditor = settings.edit();
        localEditor.putString(key, value);
        localEditor.commit();
    }

    protected boolean getSharePreferenceBoolean(String key) {
        SharedPreferences settings = this.getSharedPreferences(SHAREPRE_NAME, 0);
        boolean value  = settings.getBoolean(key, false);
        return value;
    }

    protected boolean getSharePreferenceBoolean(String key,boolean defaultValue) {
        SharedPreferences settings = this.getSharedPreferences(SHAREPRE_NAME, 0);
        boolean value  = settings.getBoolean(key, defaultValue);
        return value;
    }
    protected void putSharePreference(String key, boolean value) {
        SharedPreferences settings = this.getSharedPreferences(SHAREPRE_NAME, 0);
        SharedPreferences.Editor localEditor = settings.edit();
        localEditor.putBoolean(key, value);
        localEditor.commit();
    }

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    protected int getDisplayHeight(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }
    //获取屏幕的长度
    protected int getDisplayWidth(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

//	/**
//	 * String 返回值类型，可以自己修改
//	 * @author Administrator
//	 *
//	 * @param <Params>
//	 */
//	protected abstract class MyHttpTask<Params> extends AsyncTask<Params, Void, Object>{
//		public final AsyncTask<Params, Void, Object> executeProxy(Params... params) {
//			if(NetUtil.checkNet(BaseActivity.this)){//在每次异步前先判断网络
//				return execute(params);
//			}else{
//				PromptManager.closeProgressDialog();
//				return null;
//			}
//		}
//	}

    /**
     *动画进入下一个界面
     * @param clazz 下一个界面activity.class
     */
    protected void animNextActivity(Class<? extends Activity> clazz){
        Intent intent=new Intent();
        intent.setClass(getApplicationContext(),clazz);
        startActivity(intent);
        overridePendingTransition(R.anim.tran_next_in, R.anim.tran_next_out);//下个
    }

    /**
     *动画进入上一个界面
     * @param clazz 上一个界面activity.class
     */
    protected void animPreActivity(Class<? extends Activity> clazz){
        Intent intent=new Intent();
        intent.setClass(getApplicationContext(), clazz);
        startActivity(intent);
        overridePendingTransition(R.anim.tran_pre_in,R.anim.tran_pre_out);// 上个
    }

    public void showLoginDialog() {
        showLoginDialog("");
    }

    protected void showLoginDialog(String msg) {
        IOSAlertDialog iosAlertDialog=new IOSAlertDialog(this);
        iosAlertDialog.builder();
        iosAlertDialog.setTitle(getString(R.string.login));
        if(TextUtils.isEmpty(msg)){
            iosAlertDialog.setMsg("确认是否登录?");
        }else{
            iosAlertDialog.setMsg(msg);
        }

        iosAlertDialog.setPositiveButton(getString(R.string.login), new OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentForResult(LoginActivity.class, IntentRequstCode.activity_login);//这里
//				startIntent(LoginActivity.class);
            }
        });
        iosAlertDialog.setNegativeButton(getString(R.string.cancel), null);
        iosAlertDialog.show();
    }

    protected void startIntentForResult(Class<?> cls,int requstCode){
        startActivityForResult (new Intent(this, cls), requstCode);
    }

//    protected void startIntentForResult(Class<?> cls,int requestCode ,String locktype,int locktypeid){
//		Intent	intent=new Intent(this, cls);
////		intent.putExtra("locktype", locktype);
////		intent.putExtra("locktypeid", locktypeid);
//		startActivityForResult(intent, requestCode);
//	}

    /**************************************************分界线*************************************************/
    Dialog mProDialog;
    /**
     *
     * 显示进度框（内容，正在加载）<BR>
     * [功能详细描述]
     *
     * @param message
     *            对话框显示信息
     */
    public void showProgressDialog() {
        if (mProDialog == null) {
            if (getParent() != null) {
                mProDialog = createLoadingDialog(getParent(), "正在加载");
            } else {
                mProDialog = createLoadingDialog(this, "正在加载");
            }
        }
        showProgressDialog(mProDialog);
    }

    /**
     * 得到自定义的progressDialog
     * @param context
     * @param msg
     * @return
     */
    public static Dialog createLoadingDialog(Context context, String msg) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.loading_dialog, null);// 得到加载view
        // main.xml中的ImageView
        ImageView spaceshipImage = (ImageView) v.findViewById(R.id.img);
        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
        // 加载动画
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(
                context, R.anim.load_animation);
        // 使用ImageView显示动画
        spaceshipImage.startAnimation(hyperspaceJumpAnimation);
        tipTextView.setText(msg);// 设置加载信息

        Dialog loadingDialog = new Dialog(context, R.style.loading_dialog);// 创建自定义样式dialog

        loadingDialog.setCancelable(true);// true可以用“返回键”,false不可以用“返回键”取消
        loadingDialog.setCanceledOnTouchOutside(false);//false禁止点击屏幕取消
        loadingDialog.setContentView(v, new LinearLayout.LayoutParams(dp2Px(context, 100),
                dp2Px(context, 100)));// 设置布局
        return loadingDialog;

    }

    public static int dp2Px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     *
     * 弹出进度框<BR>
     * [功能详细描述]
     *
     * @param proDialog
     *            对话框显示信息
     */
    public void showProgressDialog(Dialog proDialog) {
        // if (!isPaused)
        // {
        proDialog.show();
        // }
    }

    public void closeProgressDialog() {
        // if (!isPaused)
        // {
        mProDialog.dismiss();
        // }
    }

    /**
     * 初始化界面控件，添加主内容界面，添加监听器等
     */
    protected abstract void initViews();
}
