package org.graffitidiary.framework.ui;

import org.graffitidiary.R;
import org.graffitidiary.utils.ActivityCollector;
import org.graffitidiary.utils.GloableParams;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class ActionBarActivity extends BaseActivity implements OnClickListener, OnLongClickListener{
    private ImageView menuView;
    private ImageView backView;
    private ImageView netView;

    private LinearLayout mLlSearch;
    private EditText mEdtSearch;
    private ImageView titleImageView;
    private TextView titleView;

    private ImageView rightView;
    private ImageView rightView2;
    private TextView rightTextView;
    private FrameLayout mFrameLayout;

    private LinearLayout mLinearLayout;
    private RelativeLayout mTopLayout;
    private ImageView bgView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_bar);
        ActivityCollector.addActivity(this);
        initNavigation();
        initContent();
        setBarLeftImg(R.drawable.icon_back);
    }

    @Override
    public void onBackPressed() {
//			regToWx();
        ActivityCollector.getInstance();
        ActivityCollector.removeActivity(this);
        finish();

    }

//	/**判断该activity是否是最后一个页面（不含主页）， 如果是则返回主页，不是则回退到上个页面*/
//	protected void onBackHome(){
//		onBackHome(-1);
//	}

    private void initContent() {
        mFrameLayout = (FrameLayout) findViewById(R.id.bar_content);
        mFrameLayout.addView(initView(mFrameLayout));
        initViews();
		/*
		 * if (UIUtil.THEME.equals("dark")) {
		 * mLinearLayout.setBackgroundColor(getResources
		 * ().getColor(R.color.app_theme_dark)); }
		 */
    }

    // 分享文字
    public void shareText(String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain");
        // 设置分享列表的标题，并且每次都显示分享列表
        startActivity(Intent.createChooser(shareIntent, "分享到"));
    }

    private void initNavigation() {
        mLinearLayout = (LinearLayout) findViewById(R.id.bar_linearlayout);
        mTopLayout = (RelativeLayout) findViewById(R.id.bar_top_layout);
        menuView = (ImageView) findViewById(R.id.bar_btn_menu);
        menuView.setOnClickListener(this);
        backView = (ImageView) findViewById(R.id.bar_btn_left);
        backView.setOnClickListener(this);
        netView = (ImageView) findViewById(R.id.bar_btn_left_net);
        netView.setOnClickListener(this);

        mLlSearch = (LinearLayout) findViewById(R.id.bar_linearlayout_search);
        mEdtSearch = (EditText) findViewById(R.id.bar_edittext_search);
        mEdtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event
                        .getAction() == KeyEvent.ACTION_DOWN)) {
                    onEditSearch();
                    return true;
                }
                return false;
            }
        });
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                onBarEditTextChanged(s.toString());
            }
        });
        titleView = (TextView) findViewById(R.id.bar_tv_title);
        titleImageView = (ImageView) findViewById(R.id.bar_img_title);
        rightView = (ImageView) findViewById(R.id.bar_btn_right);
        rightView.setOnClickListener(this);
        rightView.setOnLongClickListener(this);
        rightView2 = (ImageView) findViewById(R.id.bar_btn_right2);
        rightView2.setOnClickListener(this);

        rightTextView = (TextView) findViewById(R.id.bar_tv_right);
        rightTextView.setOnClickListener(this);
        rightTextView.setOnLongClickListener(this);
        bgView = (ImageView) findViewById(R.id.bar_bg);
		/*
		 * tipTextView = (TextView) findViewById(R.id.bar_tip); topTextView =
		 * (TextView) findViewById(R.id.bar_top);
		 */
    }

    protected View initView(ViewGroup content) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(setLayout(), content, false);
        return view;
    }

    protected void onEditSearch() {
    }

    protected void onBarEditTextChanged(String s) {
    }

    protected abstract int setLayout();

    protected abstract void initViews();

    /**
     * 设置导航条颜色
     *
     * @param color
     *            格式 Color.Red
     */
    protected void setBarColor(int color) {
        mTopLayout.setBackgroundColor(color);
    }

    protected void setBarColorIsTransparent() {
        backView.setImageResource(R.drawable.icon_back_black);
        titleView.setTextColor(getResources().getColor(R.color.black80));
        rightTextView.setTextColor(getResources().getColor(R.color.black80));
        mTopLayout.setBackgroundColor(Color.TRANSPARENT);
    }

    protected void isBarHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mLinearLayout.setPadding(0, getStatusBarHeight(), 0, 0);
        }
    }

    /**
     * 显示导航条
     */
    protected void showBar() {
        mTopLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏导航条
     */
    protected void hideBar() {
        mTopLayout.setVisibility(View.GONE);
    }
    /**通知栏透明化*/
    protected void vifrificationNotificationBar(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }
    /**通知栏消除透明化*/
    protected void defaultNotificationBardefault(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }
    /**
     * 获取返回按钮
     *
     * @return
     */
    public ImageView getBarBackView() {
        return backView;
    }

    /**
     * 获取标题控件
     *
     * @return
     */
    public TextView getBarTitleView() {
        return titleView;
    }

    public String getBarTitleText() {
        return titleView.getText() + "";
    }

    public String getBarEdtSearchText() {
        return mEdtSearch.getText().toString();
    }

    public void setBarEdtSearchText(String text) {
        mEdtSearch.setText(text);
    }

    String pageName;
    /**
     * 设置标题
     *
     * @param title
     */
    protected void setBarTitle(String title) {
        pageName = title;
        titleView.setText(title);
    }

    protected void setBarTitle(int resid) {
        pageName = getString(resid);
        titleView.setText(resid);
    }
    /**
     * 设置标题名字为logo
     * @param url 图片网络地址
     */
    protected void setBarTitleImage(String url) {
        //ImageUtil.displayImage(url, titleImageView);
        titleImageView.setVisibility(View.VISIBLE);
    }
    /**
     * 设置标题名字为logo
     * @param drawable
     */
    protected void setBarTitleImage(Drawable drawable) {
        titleImageView.setImageDrawable(drawable);
        titleImageView.setVisibility(View.VISIBLE);
    }

    protected void setBarTitleImage(int resId) {
        titleImageView.setImageResource(resId);
        titleImageView.setVisibility(View.VISIBLE);
    }

    protected void setBarTitleColor(int color) {
        titleView.setBackgroundColor(color);
    }

    /**
     * 设置导航条右边的图片按钮,设置是否显示
     *
     */
    protected void setBarRightVisibility(int visi) {
        rightView.setVisibility(visi);
    }

    /**
     * 设置导航条右边的图片按钮,设置图片
     *
     */
    protected void setBarRightImg(int resid) {
        rightView.setImageResource(resid);
    }

    /**
     * 设置导航条右边的倒数第二个图片按钮,设置图片
     *
     */
    protected void setBarRight2Img(int resid) {
        rightView2.setVisibility(View.VISIBLE);
        rightView2.setImageResource(resid);
    }



    /**
     * 设置导航条左边的图片按钮,设置图片
     *
     */
    protected void setBarLeftImg(int resid) {
        backView.setImageResource(resid);
    }

    /**
     * 设置导航条左边的网络按钮,设置图片
     *
     */
    protected void setBarLeftNetImg(int resid) {
        netView.setImageResource(resid);
    }

    /**
     * 显示网络按钮(隐藏返回按钮)
     */
    protected void showBarNetBtn() {
        backView.setVisibility(View.GONE);
        netView.setVisibility(View.VISIBLE);
    }

    /**
     * 显示菜单按钮(隐藏返回按钮)
     */
    protected void showBarMenuBtn() {
        backView.setVisibility(View.GONE);
        menuView.setVisibility(View.VISIBLE);
    }

    protected void setBarEditHint(CharSequence text) {
        mEdtSearch.setHint(text);
    }

    protected void setBarEditText(CharSequence text) {
        mEdtSearch.setText(text);
    }

    protected void setBarIsSearch() {
        mLlSearch.setVisibility(View.VISIBLE);
    }

    /**
     * 设置导航条右边的图片按钮背景,设置背景为资源ID
     *
     */
    protected void setBarRightBackgroundResource(int backgroundResource) {
        rightView.setImageResource(backgroundResource);
    }

    /**
     * 设置导航条右边的文本按钮,设置是否显示
     *
     */
    protected void setBarRightTextBtnVisibility(int visi) {
        rightTextView.setVisibility(visi);
    }

    protected void showBarRightTextBtn() {
        rightTextView.setVisibility(View.VISIBLE);
    }

    protected void showBarRightImgBtn() {
        rightView.setVisibility(View.VISIBLE);
    }

    protected void hideBarRightImgBtn() {
        rightView.setVisibility(View.GONE);
    }

    protected void hideBarRightTextBtn() {
        rightTextView.setVisibility(View.GONE);
    }

    /**
     * 设置导航条右边的文本按钮,设置文本
     *
     */
    protected void setBarRightTextBtnText(String str) {
        rightTextView.setText(str + "");
    }

    protected void setBarRightTextBtnText(int resid) {
        rightTextView.setText(resid);
    }

    protected void setBarRightTextBtnTextLoading(String str) {
        rightTextView.setText(str);
        rightTextView.getPaint().setFlags(
                Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 下面加横线
    }

    protected void setBarRightTextBtnTextLoaderr(String str) {
        rightTextView.setText(str);
        rightTextView.getPaint().setFlags(
                Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 中间加横线
    }

    protected void setBarRightTextBtnTextLoading(int resid) {
        rightTextView.setText(resid);
        rightTextView.getPaint().setFlags(
                Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 下面加横线
    }

    protected void setBarRightTextBtnTextLoaderr(int resid) {
        rightTextView.setText(resid);
        rightTextView.getPaint().setFlags(
                Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 中间加横线
    }

    protected void setBarRightTextBtnTextLoaded(String str) {
        rightTextView.setText(str);
        rightTextView.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    protected void setBarRightTextBtnTextLoaded(int resid) {
        rightTextView.setText(resid);
        rightTextView.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    /**
     * 设置导航条右边的文本按钮,设置文本颜色
     *
     */
    protected void setBarRightTextBtnTextColor(int color) {
        rightTextView.setTextColor(color);
    }

    /**
     * 获取右侧按钮,默认不显示
     *
     * @return
     */
    protected ImageView getBarRightView() {
        return rightView;
    }

    /**
     * 获取bar高度*/
    protected int getBarHeight(){
        LayoutParams params=(LayoutParams) mTopLayout.getLayoutParams();
        return params.height;
    }

    /**
     * 设置bar高度*/
    protected void setBarHeight(int height){
        LayoutParams params=(LayoutParams) mTopLayout.getLayoutParams();
        params.height=height;
    }

    /**
     * 设置bar背景*/
    protected void setBarBackground(int resid){
        mTopLayout.setBackgroundResource(resid);
    }
    /**
     * 设置barMargins
     */
    protected void setBarMargins(int left,int top,int right,int bottom){
        LayoutParams params=(LayoutParams) mTopLayout.getLayoutParams();
        params.setMargins(left, top, right, bottom);
    }
    /**
     * 获取通知栏高度*/
    protected int getStatusBarHeight(){
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 设置背景海报
     *
     * @param url
     *            图片地址
     */
    protected void setBarBg(String url) {
        //initImageLoader();
        //ImageLoader.getInstance().displayImage(url, bgView, options);
    }

    protected boolean isLogin() {
        if (GloableParams.USERID <= 0 || "".equals(GloableParams.token)
                || GloableParams.token.length() == 0) {
            return false;
        }
        return true;
    }

	/*
	 * public void showTip(String text) { tipTextView.setText(text);
	 * tipTextView.setVisibility(View.VISIBLE); }
	 *
	 * public void showTip(int textResId) { tipTextView.setText(textResId);
	 * tipTextView.setVisibility(View.VISIBLE); }
	 *
	 * public void hideTip() { tipTextView.setVisibility(View.GONE); }
	 *
	 * public void showTop(String text) { topTextView.setText(text);
	 * topTextView.setOnClickListener(this);
	 * topTextView.setVisibility(View.VISIBLE); }
	 *
	 * public void showTop(int textResId) { topTextView.setText(textResId);
	 * topTextView.setOnClickListener(this);
	 * topTextView.setVisibility(View.VISIBLE); }
	 *
	 * public void hideTop() { topTextView.setVisibility(View.GONE); }
	 */

	/*
	 * private ClickCallback callback;
	 *//**
     * 设置按钮点击回调接口
     *
     * @param callback
     */
	/*
	 * public void setClickCallback(ClickCallback callback) { this.callback =
	 * callback; }
	 *//**
     * 导航栏点击回调接口 </br>如若需要标题可点击,可再添加
     *
     * @author Asia
     *
     */
	/*
	 * public static interface ClickCallback{
	 *//**
     * 点击返回按钮回调
     */
	/*
	 * void onBackClick();
	 *
	 * void onRightClick(); }
	 */
    protected void onBarBackClick() {
        onBackPressed();
    }

    protected void onBarRightClick() {
    }

    protected void onBarTopClick() {
    }

    protected void onBarBackLongClick() {
    }

    protected void onBarRightLongClick() {
    }

    protected void onBarTopLongClick() {
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.bar_btn_menu) {
            onBarBackClick();
            return;
        }
        if (id == R.id.bar_btn_left) {
            onBarBackClick();
            return;
        }
        if (id == R.id.bar_btn_right) {
            onBarRightClick();
            return;
        }
        if (id == R.id.bar_tv_right) {
            onBarRightClick();
            return;
        }
		/*
		 * if (id == R.id.bar_top) { onBarTopClick(); return; }
		 */
    }

    @Override
    public boolean onLongClick(View v) {
        int id = v.getId();
        if (id == R.id.bar_btn_menu) {
            onBarBackLongClick();
            return true;
        }
        if (id == R.id.bar_btn_left) {
            onBarBackLongClick();
            return true;
        }
        if (id == R.id.bar_btn_right) {
            onBarRightLongClick();
            return true;
        }
        if (id == R.id.bar_tv_right) {
            onBarRightLongClick();
            return true;
        }
		/*
		 * if (id == R.id.bar_top) { onBarTopLongClick(); return true; }
		 */
        return false;
    }
}
